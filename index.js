var express = require('express');
var app = express();
var port = process.env.PORT || 8081;

app.use(express.static('src'));

var server = app.listen(port, function() {
    console.log("Listening at http://localhost:%s ", server.address().port);
});

