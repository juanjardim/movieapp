describe('Loading Directive', function () {

    var expectedHtml = [
        '<div id="floatingCirclesG">',
        '<div class="f_circleG" id="frotateG_01"></div>',
        '<div class="f_circleG" id="frotateG_02"></div>',
        '<div class="f_circleG" id="frotateG_03"></div>',
        '<div class="f_circleG" id="frotateG_04"></div>',
        '<div class="f_circleG" id="frotateG_05"></div>',
        '<div class="f_circleG" id="frotateG_06"></div>',
        '<div class="f_circleG" id="frotateG_07"></div>',
        '<div class="f_circleG" id="frotateG_08"></div>',
        '</div>'
    ].join('');

    beforeEach(module('movieApp'));

    var $rootScope, $compile;

    beforeEach(inject(function (_$rootScope_, _$compile_) {
        $rootScope = _$rootScope_;
        $compile = _$compile_;
    }));


    it('should should output the loading in HTML format', function () {
        var element = $compile('<loading></loading>')($rootScope);
        $rootScope.$digest();
        expect(element.html()).toBe(expectedHtml);
    });
});