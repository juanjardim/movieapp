describe('Results controller', function () {
    var results = {
        "Search": [
            {
                "Title": "Star Wars: Episode IV - A New Hope",
                "Year": "1977",
                "imdbID": "tt0076759",
                "Type": "movie"
            },
            {
                "Title": "Star Wars: Episode V - The Empire Strikes Back",
                "Year": "1980",
                "imdbID": "tt0080684",
                "Type": "movie"
            },
            {
                "Title": "Star Wars: Episode VI - Return of the Jedi",
                "Year": "1983",
                "imdbID": "tt0086190",
                "Type": "movie"
            }
        ]
    };
    var $controller;
    var $q;
    var $vm;
    var $rootScope;
    var $location;
    var ombApi;

    beforeEach(module('movieApp'));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_, _$location_, _omdbApi_) {
        $controller = _$controller_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $location = _$location_;
        ombApi = _omdbApi_;
    }));

    it('should load search results', function() {
        spyOn(ombApi, 'search').and.callFake(function(){
            var deferred = $q.defer();
            deferred.resolve(results);
            return deferred.promise;
        });
        $location.search('q', 'star wars');
        $vm = $controller('ResultsController');
        $rootScope.$apply();
        expect($vm.results[0].Title).toBe(results.Search[0].Title);
        expect($vm.results[1].Title).toBe(results.Search[1].Title);
        expect($vm.results[2].Title).toBe(results.Search[2].Title);
        expect(ombApi.search).toHaveBeenCalledWith('star wars');
    });

    it('should set result status to error', function() {
        spyOn(ombApi, 'search').and.callFake(function() {
            var deferred = $q.defer();
            deferred.reject();
            return deferred.promise;
        });

        $location.search('q', 'star wars');
        $vm = $controller('ResultsController');
        $rootScope.$apply();
        expect($vm.errorMessage).toBe('Something went wrong!');
    });
});