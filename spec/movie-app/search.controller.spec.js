describe('Search Controller', function () {
    var $location;
    var $controller;
    var $timeout;
    var $vm;
    beforeEach(module('movieApp'));

    beforeEach(inject(function (_$controller_, _$location_, _$timeout_) {
        $location = _$location_;
        $controller = _$controller_;
        $timeout = _$timeout_;
    }));

    it('should redirect to the query results page for non-empty query', function () {
        $vm = $controller('SearchController', {$location: $location, $timeout: $timeout}, {query: 'star wars'});
        $vm.search();
        expect($location.url()).toBe('/results?q=star%20wars');
    });

    it('should not redirect to query results for empty query', function () {
        $vm = $controller('SearchController', {$location: $location}, {query: ''});
        $vm.search();
        expect($location.url()).toBe('');
    });

    it('should redirect after 1 second of keyboard inactivity', function(){
        $vm = $controller('SearchController', {$location: $location}, {query: 'star wars'});
        $vm.keyup();
        $timeout.flush();
        expect($timeout.verifyNoPendingTasks).not.toThrow();
        expect($location.url()).toBe('/results?q=star%20wars');
    });
    
    it('should cancel timeout in keydown', function() {
        $vm = $controller('SearchController', {$location: $location}, {query: 'star wars'});
        $vm.keyup();
        $vm.keydown();
        expect($timeout.verifyNoPendingTasks).not.toThrow();
    });

    it('should cancel timeout on search', function() {
        $vm = $controller('SearchController', {$location: $location}, {query: 'star wars'});
        $vm.keyup();
        $vm.search();
        $vm.keydown();
        expect($timeout.verifyNoPendingTasks).not.toThrow();
    });
});