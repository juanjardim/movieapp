(function(){
    'use strict';

    angular.module('movieApp', [
        'ui.bootstrap',
        'omdb',
        'movieCore',
        'ngRoute'
    ]);
}());