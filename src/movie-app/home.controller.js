(function () {
    'use strict';

    angular
        .module('movieApp')
        .controller('HomeController', homeController);

    homeController.$inject = ['$interval', 'omdbApi', 'PopularMovies'];
    function homeController($interval, omdbApi, PopularMovies) {
        var results = [];
        var vm = this;
        vm.result = results[0];
        var idx = 0;

        var findMovie = function (id) {
            omdbApi.find(id)
                .then(function (data) {
                    vm.result = data;
                });
        };

       // PopularMovies.query(function() {
            var data = ['tt0076759', 'tt0080684', 'tt0086190'];
            results = data;
            findMovie(results[0]);
            $interval(function () {
                ++idx;
                findMovie(results[idx % results.length]);
            }, 5000);
        //});

    }

}());