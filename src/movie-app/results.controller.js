(function () {
    'use strict';

    angular
        .module('movieApp')
        .controller('ResultsController', resultsCtl);

    function resultsCtl($location, omdbApi) {
        var vm = this;
        var query = $location.search().q;
        omdbApi.search(query)
            .then(function (data) {
                vm.results = data.Search;
            })
            .catch(function () {
                vm.errorMessage = 'Something went wrong!';
            });

        vm.expand = function (index, id) {
            vm.results[index].loading = true;
            omdbApi.find(id)
                .then(function (data) {
                    vm.results[index].data = data;
                    vm.results[index].open = true;
                    vm.results[index].loading = false;
                });
        }
    }
}());