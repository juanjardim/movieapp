(function () {
    'use strict';

    angular
        .module('omdb')
        .factory('omdbApi', omdbApi);

    omdbApi.$inject = ['$http', '$q'];

    var baseUrl = 'http://www.omdbapi.com/?v=1&';

    function omdbApi($http, $q) {

        var search = function (query) {
            return httpPromise(baseUrl + 's=' + encodeURIComponent(query));
        };

        var find = function (id) {
            return httpPromise(baseUrl + 'i=' + id);
        };

        function httpPromise(url){
            var deferred = $q.defer();
            $http.get(url)
                .success(function(data) {
                    deferred.resolve(data);
                })
                .error(function(){
                    deferred.reject();
                });
            return deferred.promise;
        }

        return {
            search: search,
            find: find
        };
    }

}());